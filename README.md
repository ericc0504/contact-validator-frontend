# Contact Validator Frontend
This is a web application for phone number and email validation.

## System Architecture
![System Architecture](ContactValidatorArchitecture.png)

## Prerequisite
This application have to be run together with [contact-validator-api](https://bitbucket.org/ericc0504/contact-validator-api/src/master/) and [contact-validator-service](https://bitbucket.org/ericc0504/contact-validator-service/src/master/).
Please first check it out.

## Getting Started
```bash
# Clone the repo
$ git clone git@bitbucket.org:ericc0504/contact-validator-frontend.git

# Install dependencies
$ yarn

# Run
$ yarn start

# View on your favourite browser
http://localhost:3000/
```
import './App.css';
import ValidationPage from './pages/ValidationPage';
import Container from '@mui/material/Container';

import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

function App() {
  return (
    <div className='App'>
      <Container maxWidth="lg">
        <ValidationPage />
      </Container>
    </div>
  );
}

export default App;

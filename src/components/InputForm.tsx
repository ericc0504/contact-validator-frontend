import { LoadingButton } from '@mui/lab';
import { Autocomplete, Box, Grid, TextField } from '@mui/material';
import React, { FC, useState } from 'react';
import { ValidationHistory } from '../types/types';

export type InputFormProps = {
  phoneNumOption: ValidationHistory[];
  emailOption: ValidationHistory[];
  onSubmit: (phoneNum: string, email: string) => void;
  isValidating: boolean;
};

const emptyPhoneNumErrMsg = 'Please enter phone number';
const emptyEmailErrMsg = 'Please enter email';

const InputForm: FC<InputFormProps> = ({
  phoneNumOption,
  emailOption,
  onSubmit,
  isValidating,
}) => {
  const [phoneNum, setPhoneNum] = useState('');
  const [email, setEmail] = useState('');
  const [phoneNumErr, setPhoneNumErr] = useState('');
  const [emailErr, setEmailErr] = useState('');

  const renderOptionBox = (
    props: React.HtmlHTMLAttributes<HTMLElement>,
    option: ValidationHistory
  ) => {
    return (
      <Box
        component='li'
        sx={{ '& > img': { mr: 2, flexShrink: 0 } }}
        {...props}
      >
        {(option.isValid ? '✅ ' : '❌') + ` ${option.value}`}
      </Box>
    );
  };

  const onPhoneNumChange = (val: string | ValidationHistory | null) => {
    if (val) {
      if (typeof val === 'string') {
        setPhoneNum(val);
      } else {
        setPhoneNum(val.value);
      }
      setPhoneNumErr('');
    } else {
      setPhoneNum('');
      setPhoneNumErr(emptyPhoneNumErrMsg);
    }
  };

  const onEmailChange = (val: string | ValidationHistory | null) => {
    if (val) {
      if (typeof val === 'string') {
        setEmail(val);
      } else {
        setEmail(val.value);
      }
      setEmailErr('');
    } else {
      setEmail('');
      setEmailErr(emptyPhoneNumErrMsg);
    }
  };

  const emptyCheck = (): boolean => {
    let valid = true;
    if (!phoneNum) {
      valid = false;
      setPhoneNumErr(emptyPhoneNumErrMsg);
    }

    if (!email) {
      valid = false;
      setEmailErr(emptyEmailErrMsg);
    }
    return valid;
  };

  const onValidate = () => {
    if (emptyCheck()) {
      onSubmit(phoneNum, email);
    }
  };

  return (
    // <div>
    //   <Box
    //     component='form'
    //     // sx={{
    //     //   '& .MuiTextField-root': { m: 1, width: '40ch' },
    //     // }}
    //     noValidate
    //     autoComplete='off'
    //   >
    //     <div>
    <>
      <Grid container justifyContent="center">
        <Grid item xs={6} justifyContent="center">
          <Autocomplete
            id='phoneNum-entry'
            freeSolo
            disabled={isValidating}
            selectOnFocus
            options={phoneNumOption}
            getOptionLabel={(option) => option.value}
            onChange={(e, val) => onPhoneNumChange(val)}
            renderOption={renderOptionBox}
            renderInput={(params) => (
              <TextField
                {...params}
                label='Phone number'
                value={phoneNum}
                onChange={(e) => onPhoneNumChange(e.target.value)}
                helperText={phoneNumErr}
                error={phoneNumErr !== ''}
              />
            )}
          />
        </Grid>
      </Grid>
      <Grid container justifyContent="center">
        <Grid item xs={6} marginTop={2}>
          <Autocomplete
            id='email-entry'
            freeSolo
            disabled={isValidating}
            selectOnFocus
            options={emailOption}
            getOptionLabel={(option) => option.value}
            onChange={(e, val) => onEmailChange(val)}
            renderOption={renderOptionBox}
            renderInput={(params) => (
              <TextField
                {...params}
                label='Email'
                value={email}
                onChange={(e) => onEmailChange(e.target.value)}
                helperText={emailErr}
                error={emailErr !== ''}
              />
            )}
          />
        </Grid>
        <Grid container justifyContent="center">
          <Grid item xs={6}>
            <LoadingButton
              variant='contained'
              style={{ marginTop: 20, width: 140 }}
              onClick={onValidate}
              loading={isValidating}
            >
              Validate
            </LoadingButton>
          </Grid>
        </Grid>
        {/* //     </div> */}
        {/* //   </Box> */}
        {/* // </div> */}
      </Grid>
    </>
  );
};

export default InputForm;

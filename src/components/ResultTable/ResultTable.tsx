import { Alert, AlertTitle, Divider, Grid, Typography } from '@mui/material';
import { FC } from 'react';
import { GeneralResponse } from '../../network/network-types';
import { PhoneValidation, EmailValidation } from '../../types/types';
import './ResultTable.css';

export type ResultTableProps = {
  phoneNum: string;
  phoneValidationRes: GeneralResponse<PhoneValidation> | undefined;
  email: string;
  emailValidationRes: GeneralResponse<EmailValidation> | undefined;
};

const ResultTable: FC<ResultTableProps> = ({
  phoneNum,
  phoneValidationRes,
  email,
  emailValidationRes,
}) => {
  const boolToEmoji = (bool: boolean | undefined): string => {
    if (bool === true) {
      return '✅';
    } else if (bool === false) {
      return '❌';
    } else {
      return 'N/A';
    }
  };

  const renderString = (str: string | undefined): string => {
    if (str) {
      return str;
    } else {
      return 'N/A';
    }
  };

  return (
    <>
      {phoneValidationRes && emailValidationRes && (
        <Grid style={{ marginTop: 20, marginBottom: 40 }}>
          <Grid item xs={12}>
            <Divider variant='middle' />
          </Grid>
          <Grid item xs={12}>
            <Typography
              style={{ marginTop: 25, marginBottom: 15 }}
              variant='h4'
              gutterBottom
              component='div'
            >
              Result
            </Typography>
          </Grid>
          <Grid container spacing={2}>
            <Grid item xs={12} md={6} style={{ marginBottom: 40 }}>
              {!phoneValidationRes.success && (
                <Alert severity='error' style={{ marginBottom: 15 }}>
                  <AlertTitle
                    style={{ textAlign: 'start' }}
                  >{`Error - ${phoneValidationRes.error?.type}`}</AlertTitle>
                  <p style={{ textAlign: 'start' }}>
                    {phoneValidationRes.error?.info}
                  </p>
                </Alert>
              )}
              <table>
                <thead>
                  <tr>
                    <th>Phone number</th>
                    <th>{phoneNum}</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Is valid</td>
                    <td>{boolToEmoji(phoneValidationRes?.result?.valid)}</td>
                  </tr>
                  <tr>
                    <td>Local format</td>
                    <td>
                      {renderString(phoneValidationRes?.result?.local_format)}
                    </td>
                  </tr>
                  <tr>
                    <td>Intl format</td>
                    <td>
                      {renderString(
                        phoneValidationRes?.result?.international_format
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>Country prefix</td>
                    <td>
                      {renderString(phoneValidationRes?.result?.country_prefix)}
                    </td>
                  </tr>
                  <tr>
                    <td>Country code</td>
                    <td>
                      {renderString(phoneValidationRes?.result?.country_code)}
                    </td>
                  </tr>
                  <tr>
                    <td>Country name</td>
                    <td>
                      {renderString(phoneValidationRes?.result?.country_name)}
                    </td>
                  </tr>
                  <tr>
                    <td>Location</td>
                    <td>
                      {renderString(phoneValidationRes?.result?.location)}
                    </td>
                  </tr>
                  <tr>
                    <td>Carrier</td>
                    <td>{renderString(phoneValidationRes?.result?.carrier)}</td>
                  </tr>
                  <tr>
                    <td>Line type</td>
                    <td>
                      {renderString(phoneValidationRes?.result?.line_type)}
                    </td>
                  </tr>
                </tbody>
              </table>
            </Grid>
            <Grid item xs={12} md={6}>
              {!emailValidationRes.success && (
                <Alert severity='error' style={{ marginBottom: 15 }}>
                  <AlertTitle
                    style={{ textAlign: 'start' }}
                  >{`Error - ${emailValidationRes.error?.type}`}</AlertTitle>
                  <p style={{ textAlign: 'start' }}>
                    {emailValidationRes.error?.info}
                  </p>
                </Alert>
              )}
              <table>
                <thead>
                  <tr>
                    <th>Email</th>
                    <th>{email}</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Possible fix</td>
                    <td>
                      {renderString(emailValidationRes?.result?.did_you_mean)}
                    </td>
                  </tr>
                  <tr>
                    <td>Format valid</td>
                    <td>
                      {boolToEmoji(emailValidationRes?.result?.format_valid)}
                    </td>
                  </tr>
                  <tr>
                    <td>MX found</td>
                    <td>{boolToEmoji(emailValidationRes?.result?.mx_found)}</td>
                  </tr>
                  <tr>
                    <td>SMTP check</td>
                    <td>
                      {boolToEmoji(emailValidationRes?.result?.smtp_check)}
                    </td>
                  </tr>
                  <tr>
                    <td>Able to receive all emails</td>
                    <td>
                      {boolToEmoji(emailValidationRes?.result?.catch_all)}
                    </td>
                  </tr>
                  <tr>
                    <td>Role</td>
                    <td>{boolToEmoji(emailValidationRes?.result?.role)}</td>
                  </tr>
                  <tr>
                    <td>Disposable</td>
                    <td>
                      {boolToEmoji(emailValidationRes?.result?.disposable)}
                    </td>
                  </tr>
                  <tr>
                    <td>Free email account</td>
                    <td>{boolToEmoji(emailValidationRes?.result?.free)}</td>
                  </tr>
                  <tr>
                    <td>Score</td>
                    <td>{emailValidationRes?.result?.score ?? 'N/A'}</td>
                  </tr>
                </tbody>
              </table>
            </Grid>
          </Grid>
        </Grid>
      )}
    </>
  );
};

export default ResultTable;

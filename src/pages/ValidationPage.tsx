import React, { FC, useState } from 'react';
import { Grid, Typography } from '@mui/material';
import { validateEmail, validatePhoneNum } from '../network/api-client';
import { ValidationHistory } from '../types/types';
import InputForm from '../components/InputForm';
import ResultTable from '../components/ResultTable/ResultTable';
import { PhoneValidation, EmailValidation } from '../types/types';
import { updateHistory } from '../utils/utils';
import { GeneralResponse } from '../network/network-types';

const ValidationPage: FC = () => {
  const [phoneNum, setPhoneNum] = useState('');
  const [email, setEmail] = useState('');
  const [isValidating, setIsValidating] = useState(false);
  const [phoneNumOption, setPhoneNumOption] = useState<ValidationHistory[]>([]);
  const [emailOption, setEmailOption] = useState<ValidationHistory[]>([]);

  const [phoneValidationRes, setPhoneValidationRes] =
    useState<GeneralResponse<PhoneValidation>>();
  const [emailValidationRes, setEmailValidationRes] =
    useState<GeneralResponse<EmailValidation>>();

  const onSubmit = (phoneNum: string, email: string) => {
    setIsValidating(true);
    Promise.all([validatePhoneNum(phoneNum), validateEmail(email)])
      .then((values) => {
        setPhoneNum(phoneNum);
        setEmail(email);
        setPhoneValidationRes(values[0]);
        setEmailValidationRes(values[1]);
        setPhoneNumOption(
          updateHistory(phoneNumOption, {
            value: phoneNum,
            isValid: values[0].success && (values[0].result?.valid === true),
          })
        );
        setEmailOption(
          updateHistory(emailOption, { value: email, 
            isValid: values[1].success && (values[1].result?.format_valid === true) })
        );
        setIsValidating(false);
      })
      .catch((e) => {
        console.log(e);
        setIsValidating(false);
      });
  };
  return (
    <Grid>
      <Grid item xs={12}>
        <Typography
          variant='h3'
          gutterBottom
          component='div'
          style={{ marginTop: 20 }}
        >
          Contact Validator
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <InputForm
          phoneNumOption={phoneNumOption}
          emailOption={emailOption}
          isValidating={isValidating}
          onSubmit={onSubmit}
        />
      </Grid>
      <ResultTable
        phoneNum={phoneNum}
        phoneValidationRes={phoneValidationRes}
        email={email}
        emailValidationRes={emailValidationRes}
      />
    </Grid>
  );
};

export default ValidationPage;

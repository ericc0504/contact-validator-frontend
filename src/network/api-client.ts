import { EmailValidation, PhoneValidation } from '../types/types';
import axios from 'axios';
import { GeneralResponse } from './network-types';

const apiUrl = process.env.REACT_APP_API_BASE_URL;

export async function validatePhoneNum(
  phoneNum: string
): Promise<GeneralResponse<PhoneValidation>> {
  const url = `${apiUrl}/validatePhone?phoneNum=${phoneNum}`;
  const res = await axios.get(url);
  console.log(res.data);
  return res.data;

  // return new Promise((res, rej) => {
  //   setTimeout(() => {
  //     res({
  //       success: true,
  //       result: {
  //         valid: true,
  //         number: '51892222',
  //         local_format: '892222',
  //         international_format: '+51892222',
  //         country_prefix: '+51',
  //         country_code: 'PE',
  //         country_name: 'Peru',
  //         location: '',
  //         carrier: '',
  //         line_type: 'special_services',
  //       },
  //     });
  //   }, 100);
  // });
}

export async function validateEmail(
  email: string
): Promise<GeneralResponse<EmailValidation>> {
  const url = `${apiUrl}/validateEmail?email=${email}`;
  const res = await axios.get(url);
  console.log(res.data);
  return res.data;

  // return new Promise((res, rej) => {
  //   setTimeout(() => {
  //     res({
  //       success: false,
  //       result: undefined,
  //       error: {
  //         code: 211,
  //         type: 'format_not_valid',
  //         info: '',
  //       },
  //     });
  //   }, 100);
  // });
}

export type Error = {
  code: Number;
  type: string;
  info: string;
};

export class GeneralResponse<T> {
  success: boolean;
  result?: T | undefined;
  error?: Error;

  constructor(success: boolean, result?: T, error?: any) {
    this.success = success;
    this.result = result;
    this.error = error;
  }
}

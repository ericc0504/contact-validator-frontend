export type ValidationHistory = {
  value: string;
  isValid: boolean;
};

export type PhoneValidation = {
  valid: boolean;
  number: string;
  local_format: string;
  international_format: string;
  country_prefix: string;
  country_code: string;
  country_name: string;
  location: string;
  carrier: string;
  line_type: string;
};

export type EmailValidation = {
  email: string;
  did_you_mean: string;
  format_valid: boolean;
  mx_found: boolean;
  smtp_check: boolean;
  catch_all: boolean;
  role: boolean;
  disposable: boolean;
  free: boolean;
  score: Number;
};

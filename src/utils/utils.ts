import { ValidationHistory } from '../types/types';

export function updateHistory(
  current: ValidationHistory[],
  newRecord: ValidationHistory
): ValidationHistory[] {
  if (!current) {
    return [newRecord];
  } else {
    let recordExist = false;
    for (let i = 0; i < current.length; i++) {
      if (current[i].value === newRecord.value) {
        current[i] = newRecord;
        recordExist = true;
      }
    }
    if (!recordExist) {
      current = [newRecord, ...current];
    }
    return current;
  }
}
